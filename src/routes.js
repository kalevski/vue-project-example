import HomePage from './page/HomePage.vue'
import OtherPage from './page/OtherPage.vue'

export const routes = [
  {
    path: '/',
    name: 'home',
    component: HomePage,
  }, {
    path: '/other',
    name: 'other',
    component: OtherPage,
  }
]